<?php

	class Base_View {

		public function __construct() { }

		#Renderizar las vistas
    public function render($name) {
      require_once("views/layouts/cabecera.php");
      require_once("views/$name.php");
      require_once("views/layout/pie.php");
		}
	}